require("dotenv").config()
import * as bodyParser from "body-parser"
import { Server } from "@overnightjs/core"
import { createConnection, Connection } from "typeorm"
import VideosController from "./routes/videos"
import RatingsController from "./routes/ratings"
import cors from "cors"
import CommentsController from "./routes/comments"

export class TwentyFourGServer extends Server {
  constructor() {
    super(process.env.NODE_ENV === "development")
    this.app.use(cors())
    this.app.use(bodyParser.json())
    this.app.use(bodyParser.urlencoded({ extended: true }))
    this.setupControllers()
  }

  private setupControllers(): void {
    super.addControllers([
      new VideosController(),
      new RatingsController(),
      new CommentsController(),
    ])
    console.log("Setup controllers.")
  }

  public async setupDatabase(): Promise<void> {
    const connection: Connection = await createConnection()
    console.log("Setup database connections.")
    console.log("Running database migrations…")
    const migrations = await connection.runMigrations()
    console.log("Finished database migrations.", migrations)
  }

  public start(port: number): void {
    this.app.listen(port, () => {
      console.log("Server listening on port: " + port)
    })
  }
}

try {
  const server = new TwentyFourGServer()
  server.setupDatabase().then(() => {
    const port = process.env.PORT ? parseInt(process.env.PORT) : 8080
    server.start(port)
  })
} catch (error) {
  console.error(error)
  console.error("Failed to start server.")
}
