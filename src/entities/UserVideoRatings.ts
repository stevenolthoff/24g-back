import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeUpdate,
  ManyToOne,
  JoinColumn,
} from "typeorm"
import Users from "./Users"
import Videos from "./Videos"

export enum Rating {
  thumbs_up = "thumbs_up",
  thumbs_down = "thumbs_down",
}

@Entity({ name: "user_video_ratings" })
export default class UserVideoRatings extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @ManyToOne((type) => Users, (user) => user.id)
  @JoinColumn({ name: "user_id" })
  user: Users

  @ManyToOne((type) => Videos, (video) => video.id)
  @JoinColumn({ name: "video_id" })
  video: Videos

  @Column("enum", { enum: ["thumbs_up", "thumbs_down"] })
  rating: Rating

  @Column("timestamptz", {
    default: () => "CURRENT_TIMESTAMP",
  })
  created_at: Date

  @Column("timestamptz", {
    default: () => "CURRENT_TIMESTAMP",
  })
  updated_at: Date

  @BeforeUpdate()
  onBeforeUpdate() {
    this.updated_at = new Date()
  }

  constructor(userVideoRating: Partial<UserVideoRatings>) {
    super()
    Object.assign(this, userVideoRating)
  }

  static findOneByIds(userId: number, videoId: number) {
    return this.createQueryBuilder("user_video_ratings")
      .leftJoinAndSelect("user_video_ratings.user", "user")
      .leftJoinAndSelect("user_video_ratings.video", "video")
      .where("user.id = :userId", { userId })
      .andWhere("video.id = :videoId", { videoId })
      .getOne()
  }

  static async insertWithIds(userId: number, videoId: number, rating: Rating) {
    const [user, video] = await Promise.all([
      Users.findOne(userId),
      Videos.findOne(videoId),
    ])
    const userVideoRating = new UserVideoRatings({
      user,
      video,
      rating,
    })
    return userVideoRating.save()
  }

  static async getRatingCounts(videoId: number) {
    const video = await Videos.findOne(videoId)
    if (!video) {
      throw new Error("Video not found")
    }
    const [thumbs_up, thumbs_down] = await Promise.all([
      UserVideoRatings.count({
        where: {
          video,
          rating: Rating.thumbs_up,
        },
      }),
      UserVideoRatings.count({
        where: {
          video,
          rating: Rating.thumbs_down,
        },
      }),
    ])
    return {
      thumbs_up,
      thumbs_down,
    }
  }
}
