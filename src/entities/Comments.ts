import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeUpdate,
  ManyToOne,
  JoinColumn,
} from "typeorm"
import Videos from "./Videos"
import Users from "./Users"

@Entity({ name: "comments" })
export default class Comments extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column("text")
  content: string

  @ManyToOne((type) => Videos, (video) => video.id, { nullable: false })
  @JoinColumn({ name: "video_id" })
  video: Videos

  @ManyToOne((type) => Users, (user) => user.id, { nullable: false })
  @JoinColumn({ name: "user_id" })
  user: Users

  @Column("timestamptz", {
    default: () => "CURRENT_TIMESTAMP",
  })
  created_at: Date

  @Column("timestamptz", {
    default: () => "CURRENT_TIMESTAMP",
  })
  updated_at: Date

  @BeforeUpdate()
  onBeforeUpdate() {
    this.updated_at = new Date()
  }

  constructor(comment: Partial<Comments>) {
    super()
    Object.assign(this, comment)
  }

  static findByVideoId(videoId: number) {
    return this.createQueryBuilder("comments")
      .leftJoinAndSelect("comments.video", "video")
      .leftJoinAndSelect("comments.user", "user")
      .where("video.id = :videoId", { videoId })
      .getMany()
  }

  static async insertWithIds(userId: number, videoId: number, content: string) {
    const [user, video] = await Promise.all([
      Users.findOne(userId),
      Videos.findOne(videoId),
    ])
    const userVideoRating = new Comments({
      user,
      video,
      content,
    })
    return userVideoRating.save()
  }
}
