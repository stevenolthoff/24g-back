import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeUpdate,
} from "typeorm"

@Entity({ name: "videos" })
export default class Videos extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column("text")
  title: string

  @Column("text")
  video: string

  @Column("text")
  thumb: string

  @Column("int", { default: 0 })
  views: number

  @Column("timestamptz", {
    default: () => "CURRENT_TIMESTAMP",
  })
  created_at: Date

  @Column("timestamptz", {
    default: () => "CURRENT_TIMESTAMP",
  })
  updated_at: Date

  @BeforeUpdate()
  onBeforeUpdate() {
    this.updated_at = new Date()
  }

  constructor(video: Partial<Videos>) {
    super()
    Object.assign(this, video)
  }

  public incrementView() {
    this.views += 1
    return this.save()
  }
}
