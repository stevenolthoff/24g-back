import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeUpdate,
} from "typeorm"

@Entity({ name: "users" })
export default class Users extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column("text")
  username: string

  @Column("text", { nullable: true })
  picture_url: string

  @Column("timestamptz", {
    default: () => "CURRENT_TIMESTAMP",
  })
  created_at: Date

  @Column("timestamptz", {
    default: () => "CURRENT_TIMESTAMP",
  })
  updated_at: Date

  @BeforeUpdate()
  onBeforeUpdate() {
    this.updated_at = new Date()
  }

  constructor(user: Partial<Users>) {
    super()
    Object.assign(this, user)
  }
}
