import { OK, BAD_REQUEST } from "http-status-codes"
import { Controller, Get, Put, Post } from "@overnightjs/core"
import { Request, Response } from "express"
import Videos from "../entities/Videos"
import UserVideoRatings from "../entities/UserVideoRatings"

@Controller("videos")
export default class VideosController {
  @Get("/")
  private async getVideos(request: Request, response: Response) {
    try {
      let videos: Videos[] = await Videos.find()
      return response.json({ videos })
    } catch (error) {
      console.error(error)
      return response.status(BAD_REQUEST).json({ message: "error" })
    }
  }
  @Get(":id")
  private async getVideo(request: Request, response: Response) {
    try {
      const videoId: number = parseInt(request.params.id)
      let video = await Videos.findOne(videoId)
      if (video) {
        video.incrementView()
      }
      return response.json({ video })
    } catch (error) {
      console.error(error)
      return response.status(BAD_REQUEST).json({ message: "error" })
    }
  }
}
