import { OK, BAD_REQUEST } from "http-status-codes"
import { Controller, Get, Put, Post, Delete } from "@overnightjs/core"
import { Request, Response } from "express"
import Videos from "../entities/Videos"
import UserVideoRatings, { Rating } from "../entities/UserVideoRatings"

@Controller("")
export default class RatingsController {
  @Get("videos/:id/rating")
  private async getVideoRating(request: Request, response: Response) {
    try {
      const counts = await UserVideoRatings.getRatingCounts(
        parseInt(request.params.id)
      )
      return response.json({ counts })
    } catch (error) {
      console.error(error)
      return response.status(BAD_REQUEST).json({ message: "error" })
    }
  }
  @Get("users/:userId/videos/:videoId/rating")
  private async getUserVideoRating(request: Request, response: Response) {
    try {
      const userId: number = parseInt(request.params.userId)
      const videoId: number = parseInt(request.params.videoId)
      const userVideoRating = await UserVideoRatings.findOneByIds(
        userId,
        videoId
      )
      return response.json({ userVideoRating })
    } catch (error) {
      console.error(error)
      return response.status(BAD_REQUEST).json({ message: "error" })
    }
  }
  @Put("users/:userId/videos/:videoId/rating")
  private async putUserVideoRating(request: Request, response: Response) {
    try {
      const userId: number = parseInt(request.params.userId)
      const videoId: number = parseInt(request.params.videoId)
      const ratingString = String(request.body.rating)
      if (!["thumbs_up", "thumbs_down"].includes(ratingString)) {
        throw new Error("Invalid rating")
      }
      const rating: Rating = (<any>Rating)[ratingString]
      let userVideoRating = await UserVideoRatings.findOneByIds(userId, videoId)
      if (userVideoRating) {
        userVideoRating.rating = rating
        userVideoRating = await userVideoRating.save()
      } else {
        userVideoRating = await UserVideoRatings.insertWithIds(
          userId,
          videoId,
          rating
        )
      }
      const counts = await UserVideoRatings.getRatingCounts(videoId)
      return response.json({ userVideoRating, counts })
    } catch (error) {
      console.error(error)
      return response.status(BAD_REQUEST).json({ message: "error" })
    }
  }
  @Delete("users/:userId/videos/:videoId/rating")
  private async deleteUserVideoRating(request: Request, response: Response) {
    try {
      const userId: number = parseInt(request.params.userId)
      const videoId: number = parseInt(request.params.videoId)
      let userVideoRating = await UserVideoRatings.findOneByIds(userId, videoId)
      userVideoRating = await userVideoRating.remove()
      const counts = await UserVideoRatings.getRatingCounts(videoId)
      return response.json({ userVideoRating, counts })
    } catch (error) {
      console.error(error)
      return response.status(BAD_REQUEST).json({ message: "error" })
    }
  }
}
