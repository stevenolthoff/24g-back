import { BAD_REQUEST } from "http-status-codes"
import { Controller, Get, Put } from "@overnightjs/core"
import { Request, Response } from "express"
import Comments from "../entities/Comments"

@Controller("")
export default class CommentsController {
  @Get("videos/:id/comments")
  private async getVideoComments(request: Request, response: Response) {
    try {
      const comments = await Comments.findByVideoId(parseInt(request.params.id))
      return response.json({ comments })
    } catch (error) {
      console.error(error)
      return response.status(BAD_REQUEST).json({ message: "error" })
    }
  }
  @Put("users/:userId/videos/:videoId/comments")
  private async putVideoComment(request: Request, response: Response) {
    try {
      const userId = parseInt(request.params.userId)
      const videoId = parseInt(request.params.videoId)
      const content: string = request.body.content
      const comment = await Comments.insertWithIds(userId, videoId, content)
      return response.json({ comment })
    } catch (error) {
      console.error(error)
      return response.status(BAD_REQUEST).json({ message: "error" })
    }
  }
}
