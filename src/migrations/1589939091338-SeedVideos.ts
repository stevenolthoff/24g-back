import { MigrationInterface, QueryRunner, getRepository } from "typeorm"
import Videos from "../entities/Videos"

const playlist: Partial<Videos>[] = [
  {
    id: 1,
    title: "Who is 24G?",
    video:
      "https://static-email-hosting.s3.amazonaws.com/24G_Test_Project/videos/who_is_24g.mp4",
    thumb:
      "https://static-email-hosting.s3.amazonaws.com/24G_Test_Project/videos/who_is_24g.jpg",
    views: 22343900,
  },
  {
    id: 2,
    title: "CES Overview",
    video:
      "https://static-email-hosting.s3.amazonaws.com/24G_Test_Project/videos/ces_overview.mp4",
    thumb:
      "https://static-email-hosting.s3.amazonaws.com/24G_Test_Project/videos/ces_overview.jpg",
    views: 8442,
  },
  {
    id: 3,
    title: "Future of Drones",
    video:
      "https://static-email-hosting.s3.amazonaws.com/24G_Test_Project/videos/future_of_drones.mp4",
    thumb:
      "https://static-email-hosting.s3.amazonaws.com/24G_Test_Project/videos/future_of_drones.jpg",
    views: 548548,
  },
]

export class SeedVideos1589939091338 implements MigrationInterface {
  public async up(queryRunner: QueryRunner) {
    return Promise.all(
      playlist.map((video) => {
        const newVideo = new Videos(video)
        return newVideo.save()
      })
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
