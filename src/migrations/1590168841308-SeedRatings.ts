import { MigrationInterface, QueryRunner } from "typeorm"
import UserVideoRatings, { Rating } from "../entities/UserVideoRatings"

export class SeedRatings1590168841308 implements MigrationInterface {
  public async up(queryRunner: QueryRunner) {
    return UserVideoRatings.insertWithIds(1, 1, Rating.thumbs_up)
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
