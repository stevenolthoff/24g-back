import { MigrationInterface, QueryRunner } from "typeorm"
import Comments from "../entities/Comments"

export class SeedComments1590167946704 implements MigrationInterface {
  public async up(queryRunner: QueryRunner) {
    return Promise.all([
      Comments.insertWithIds(2, 1, "Eat it!"),
      Comments.insertWithIds(1, 1, "Pretty good."),
      Comments.insertWithIds(
        1,
        2,
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
      ),
    ])
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
