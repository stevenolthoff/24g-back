import { MigrationInterface, QueryRunner } from "typeorm"
import Users from "../entities/Users"

const users: Partial<Users>[] = [
  {
    id: 1,
    username: "steven",
    picture_url:
      "https://stevenolthoff-assets.s3.us-east-2.amazonaws.com/me.jpeg",
  },
  {
    id: 2,
    username: "weirdal",
    picture_url:
      "https://stevenolthoff-assets.s3.us-east-2.amazonaws.com/weirdal.jpg",
  },
]

export class SeedUsers1590000110644 implements MigrationInterface {
  public async up(queryRunner: QueryRunner) {
    return Promise.all(
      users.map((user) => {
        const newUser = new Users(user)
        return newUser.save()
      })
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
